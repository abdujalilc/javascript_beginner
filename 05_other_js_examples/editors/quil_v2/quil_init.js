window.onload = function () {
    if (typeof Quill !== 'undefined') {
        const toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'],
            [{ 'list': 'ordered' }, { 'list': 'bullet' }],
            [{ 'header': [1, 2, false] }],
            [{ 'font': [] }],
            [{ 'size': ['small', false, 'large', 'huge'] }],
            [{ 'color': [] }, { 'background': [] }],
            [{ 'align': [] }],
            ['clean'],
            ['image'],
            ['link'],
            ['blockquote'],
            ['code-block'],
            [{ 'direction': 'rtl' }]
        ];

        const quill = new Quill('#editor', {
            theme: 'snow',
            modules: {
                toolbar: toolbarOptions,
                imageResize: {
                    modules: ['Resize', 'DisplaySize']
                }
            }
        });

        // Real-time HTML update
        const htmlView = document.getElementById('html-view');
        quill.on('text-change', () => {
            htmlView.value = quill.root.innerHTML; // Return HTML only
        });

        // Initialize HTML view with existing content
        htmlView.value = quill.root.innerHTML;
    } else {
        console.error('Quill is not defined.');
    }
};