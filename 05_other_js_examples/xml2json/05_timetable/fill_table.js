function UpdateTable(tblTime, classroomsData) {
  let table = document.getElementById(tblTime);
  table.innerHTML = '';

  var tableHTML = "<tr>";
  tableHTML += "<th>Rooms</th>";
  for (let i = 0; i < periods.length; i++) {
    tableHTML += "<th>" + periods[i]._starttime + " - " + periods[i]._endtime + "</th>";
  }
  tableHTML += "</tr>";
  for (let k = 0; k < classroomsData.length; k++) {
    tableHTML += "<tr>";
    var cardElement = classroomsData[k];
    tableHTML += `<td><div class="wu-cell">${cardElement.roomName}</div></td>`;
    for (let j = 0; j < periods.length; j++) {
      if (cardElement?.lessons[j]) {
        let group = cardElement?.lessons[j]?.group != '' ? cardElement?.lessons[j]?.group + '<br>' : ''
        let teacher = cardElement?.lessons[j]?.teacher != '' ? cardElement?.lessons[j]?.teacher + '<br>' : ''
        let module = cardElement?.lessons[j]?.module != '' ? cardElement?.lessons[j]?.module + '<br>' : ''
        tableHTML += `<td><div class="wu-cell">
      ${group}
      ${teacher}
      ${module}
      </div></td>`;
      }
      else tableHTML += `<td><div class="wu-cell"></div></td>`;
    }
    tableHTML += "</tr>";
  }

  table.innerHTML = tableHTML;
}
