function FillWeekDaySelect(jsonObj) {
    var days = jsonObj.timetable.daysdefs.daysdef.slice(2);
    let selectDay = document.getElementById('selectDay');
    const currentWeekDayIndex = new Date().getDay();
    if (days) {
        days.map((day, i) => {
            let opt = document.createElement("option");
            opt.value = day._days;
            opt.innerHTML = day._name;
            if (i == currentWeekDayIndex - 1)
                opt.selected = true;
            selectDay.append(opt);
        });
    }
}

/*var periods = jsonObj.timetable.periods;
let selectHour = document.getElementById('selectPeriod');
if (periods) {
periods.period.map((item, i) => {
    let opt = document.createElement("option");
    opt.value = item._period;
    opt.innerHTML = item._starttime + '-' + item._endtime;
    selectHour.append(opt);
});
}*/