function GenerateDataForTable() {
    let currentDay = selectDay.options[selectDay.selectedIndex].value;
    console.log("currentDay", currentDay);

    let filteredCardsByDay = cards.filter((x) => x._days == currentDay);
    let classroomsData = [];
    for (let i = 0; i < classrooms.length; i++) {
        classroomsData.push({
            [classrooms[i]._id]: [],
            day: currentDay,
            roomName: classrooms[i]._name,
            lessons: []
        });
        for (let j = 0; j < periods.length; j++) {
            let filteredCardsByPeriod = filteredCardsByDay.filter((x) => x._period == periods[j]._period);
            let isClassroomBusy = filteredCardsByPeriod.find((z) => z._classroomids == classrooms[i]._id);

            if (isClassroomBusy) {
                let lesson = lessons.find((x) => x._id == isClassroomBusy._lessonid);
                let group = classes.find((x) => x._id == lesson._classids)?._name ?? '';
                let teacher = teachers.find((x) => x._id == lesson._teacherids)?._name ?? '';
                let module = subjects.find((x) => x._id == lesson._subjectid)?._name ?? '';
                let classProcess = { group: group, teacher: teacher, module: module };
                classroomsData[i][classrooms[i]._id].push(periods[j]);
                classroomsData[i].lessons.push(classProcess);
            }
            else {
                classroomsData[i][classrooms[i]._id].push("");
                classroomsData[i].lessons.push("");
            }
        }
    }
    return classroomsData;
}