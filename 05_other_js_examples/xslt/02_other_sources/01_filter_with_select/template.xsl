<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <script type="text/javascript">
                    var showList = function() {
                    document.getElementById("swarms").style.visibility = "visible";
                    <xsl:variable name="thisOne" select="'document.allCities.select.options[document.allCities.select.selectedIndex].value'" />
                    }
                </script>
            </head>
            <body>
                <!-- Display list of cities -->
                <div id="city_list">
                    <xsl:variable name="unique-list" select="swarmlist/member/cities/city[not(.=following::city)]" />
                    <form name="allCities">
                        <strong>Select A City and Click the Button to Display the List: </strong>
                        <select name="select">
                            <option value="all">All</option>
                            <xsl:for-each select="$unique-list">
                                <xsl:sort select="."/>
                                <option>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="."/>
                                    </xsl:attribute>
                                    <xsl:value-of select="."/>
                                </option>
                            </xsl:for-each>
                        </select>
                        <input name="submit" type="button" value="Display" onclick="showList();" />
                    </form>
                </div>
                <!-- End of City list -->

                <!-- Display list of volunteers -->
                <table border="1" id="swarms" style="visibility:hidden">
                    <tr bgcolor="#9acd32">
                        <th>Name</th>
                        <th>Phone</th>
                    </tr>
                    <xsl:for-each select="swarmlist/member">
                        <xsl:choose>
                            <xsl:when test="cities/city = $thisOne">
                                <tr>
                                    <td>
                                        <xsl:value-of select="name"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="phone"/>
                                    </td>
                                </tr>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>
                </table>
                <!-- End of volunteer list -->
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet> 