<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <table border="1">
            <xsl:apply-templates select="//url" />
        </table>
    </xsl:template>
    <xsl:template match="url">
        <xsl:variable name="id" select="@bloggerId" />
        <xsl:if test="count(//bloggers//name[@bloggerId = $id]) &gt; 0">
            <tr>
                <td>
                    <xsl:value-of select="//bloggers//name[@bloggerId = $id]" />
                </td>
                <td>
                    <xsl:value-of select="." />
                </td>
            </tr>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>