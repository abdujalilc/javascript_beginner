<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>

    <xsl:template match="/*">
        <table border="1">
            <thead>
                <th>Site</th>
                <th>Blogger</th>
            </thead>
            <tbody>
                <xsl:apply-templates/>
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match=
  "url[@bloggerId = /*/bloggers/name/@bloggerId]">
        <tr>
            <td>
                <a href="{.}">
                    <xsl:value-of select="."/>
                </a>
            </td>
            <td>
                <xsl:value-of select=
     "/*/bloggers/name[@bloggerId = current()/@bloggerId]"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="text()"/>
</xsl:stylesheet>