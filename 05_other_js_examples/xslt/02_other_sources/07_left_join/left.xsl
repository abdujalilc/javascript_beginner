<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/Export">
        <html>
            <body>

                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Master ID</th>
                        <th>Master Name</th>
                        <th>Detail</th>
                    </tr>
                    <xsl:for-each select="Master">
                        <xsl:variable name="mid" select="MASTER_ID"/>
                        <tr>
                            <td>
                                <xsl:value-of select="MASTER_ID"/>
                            </td>
                            <td>
                                <xsl:value-of select="MASTER_NAME"/>
                            </td>
                            <!--The following solves the problem...-->
                            <td>
                                <xsl:variable name="data" select="../Detail[MASTER_ID=$mid]/DETAIL_DATA"/>
                                <xsl:choose>
                                    <xsl:when test="$data">
                                        <xsl:value-of select="$data"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>&#160;</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>