<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <select>
      <option>Select Country</option>
      <xsl:for-each select="catalog/cd/country[not(.=preceding::*)]">
        <option>
          <xsl:value-of select="."/>
        </option>
      </xsl:for-each>
    </select>
  </xsl:template>
</xsl:stylesheet>