<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="root">
        <xsl:call-template name="person_order" />
    </xsl:template>

    <xsl:template name="person_order">
        <orders>
            <xsl:for-each select="//Person">
                <xsl:variable name ="pid" select="@id" />
                <xsl:call-template name="left_join">
                    <xsl:with-param name="jname" select="'order'"/>
                    <xsl:with-param name="left" select="."/>
                    <xsl:with-param name="right" select="//Order[P_Id = $pid]"/>
                </xsl:call-template>
            </xsl:for-each>
        </orders>
    </xsl:template>


    <xsl:template name="left_join">
        <xsl:param name="jname" />
        <xsl:param name="left" />
        <xsl:param name="right" />

        <xsl:choose>
            <xsl:when test="$right">
                <xsl:for-each select="$right">
                    <xsl:call-template name="print_join">
                        <xsl:with-param name="jname" select="$jname"/>
                        <xsl:with-param name="left" select="$left"/>
                        <xsl:with-param name="right" select="."/>
                    </xsl:call-template>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="print_join">
                    <xsl:with-param name="jname" select="$jname"/>
                    <xsl:with-param name="left" select="$left"/>

                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template name="print_join">
        <xsl:param name="jname" />
        <xsl:param name="left" />
        <xsl:param name="right" />
        <xsl:element name="{$jname}">
            <xsl:for-each select="$left">
                <xsl:apply-templates select="node() "/>
            </xsl:for-each>
            <xsl:if test="$right">
                <xsl:for-each select="$right">
                    <xsl:apply-templates select="node() "/>
                </xsl:for-each>
            </xsl:if>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>