<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <ul class="gta-2">
            <xsl:apply-templates select="/Properties/projects" />
        </ul>
        <ul class="gta-2">
            <xsl:apply-templates select="/Properties/projects"/>
        </ul>
    </xsl:template>
    <xsl:template match = "project">
        <ul class="{@field}">
            <xsl:apply-templates select="projectValue"/>
        </ul>
    </xsl:template>

    <xsl:template match = "projectValue">
        <li>
            <xsl:value-of select="." />
        </li>
    </xsl:template>
</xsl:stylesheet>