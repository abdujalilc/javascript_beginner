<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:param name="type" select="'value3'" />
    <xsl:template match="/">
        <xsl:apply-templates
            select="/*/product[@*[name()=$type and (.='true' or .='1')]]" />
    </xsl:template>
    <xsl:template match="product">        
        <div>
            <xsl:value-of select="@id" />
            <!-- <xsl:text>&#xA;</xsl:text> -->
        </div>        
    </xsl:template>
</xsl:stylesheet>