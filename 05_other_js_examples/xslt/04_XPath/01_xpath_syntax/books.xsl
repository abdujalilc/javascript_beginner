<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
    <xsl:text>/bookstore/book[1]/year: </xsl:text>
        <xsl:value-of select="/bookstore/book[1]/year" />
        <br></br>
        <xsl:text>/bookstore/book[last()]/year: </xsl:text>
        <xsl:value-of select="/bookstore/book[last()]/year" />
        <br></br>
        <xsl:text>/bookstore/book[last()-1]/year: </xsl:text>
        <xsl:value-of select="/bookstore/book[last()-1]/year" />
        <br></br>
        <xsl:text>/bookstore/book[position &lt; 3]/year: </xsl:text>
        <xsl:value-of select="/bookstore/book[position &lt; 3]/year" />
        <br></br>
        <xsl:text>//title[@lang]: </xsl:text>
        <xsl:value-of select="//title[@lang]" />
        <br></br>
        <xsl:text>//title[@lang='en']: </xsl:text>
        <xsl:value-of select="//title[@lang='en']" />
        <br></br>
        <xsl:text>/bookstore/book[price &gt; 35.00]: </xsl:text>
        <xsl:value-of select="/bookstore/book[price &gt; 35.00]" />
        <br></br>
    </xsl:template>
</xsl:stylesheet>