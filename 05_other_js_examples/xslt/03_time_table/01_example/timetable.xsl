<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">   
        <table border="1">          
          <xsl:for-each select="timetable/cards/card">
            <tr>
              <td>
                <xsl:value-of select="@lessonid"/>
              </td>
              <td>
                <xsl:value-of select="@classroomids"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
  </xsl:template>

</xsl:stylesheet>