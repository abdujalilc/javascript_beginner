# JavaScript Beginner

This repository contains fundamental web development tutorials and examples for beginners.

## Project Structure

- **01_basics/** - Basic JavaScript concepts and syntax.
- **02_html/** - HTML basics and structure.
- **03_css/** - CSS styling and examples.
- **04_javascript/** - JavaScript examples and scripts.
- **05_other_js_examples/** - Additional JavaScript exercises.

## Learning Resources

We started learning web development tutorials mostly from [W3Schools](https://www.w3schools.com/).

## Setup & Usage

1. Clone the repository:
   ```sh
   git clone https://gitlab.com/your-repo/javascript_beginner.git
   ```
2. Open the files in your browser or a code editor.

## Contribution

Contributions are welcome! Feel free to fork the repository and submit a pull request.

## License

This project is licensed under the MIT License.
