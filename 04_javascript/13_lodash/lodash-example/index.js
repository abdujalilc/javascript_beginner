// index.js
const _ = require('lodash');

// Sample data
const users = [
    { user: 'Alice', age: 25 },
    { user: 'Bob', age: 30 },
    { user: 'Charlie', age: 35 },
    { user: 'David', age: 30 }
];

// 1. Group by age
const groupedByAge = _.groupBy(users, 'age');
console.log('Grouped by age:', groupedByAge);

// 2. Find the user with the highest age
const oldestUser = _.maxBy(users, 'age');
console.log('Oldest user:', oldestUser);

// 3. Remove duplicates based on age
const uniqueAges = _.uniqBy(users, 'age');
console.log('Unique ages:', uniqueAges);
