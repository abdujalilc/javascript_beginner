// lodash-sample.js
const _ = require('lodash');

// Sample data: an array of products
const products = [
    { id: 1, name: 'Laptop', price: 999, category: 'Electronics' },
    { id: 2, name: 'Phone', price: 499, category: 'Electronics' },
    { id: 3, name: 'Tablet', price: 299, category: 'Electronics' },
    { id: 4, name: 'Shoes', price: 89, category: 'Fashion' },
    { id: 5, name: 'T-shirt', price: 19, category: 'Fashion' }
];

// 1. Filter products by category
const electronics = _.filter(products, { category: 'Electronics' });
console.log('Electronics:', electronics);

// 2. Map to get an array of product names
const productNames = _.map(products, 'name');
console.log('Product Names:', productNames);

// 3. Find a product by its ID
const productById = _.find(products, { id: 3 });
console.log('Product with ID 3:', productById);

// 4. Get the total price of all products
const totalPrice = _.sumBy(products, 'price');
console.log('Total Price of Products:', totalPrice);

// 5. Group products by category
const groupedProducts = _.groupBy(products, 'category');
console.log('Grouped Products:', groupedProducts);
